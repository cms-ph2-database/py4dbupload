#!/usr/bin/python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 29-October-2024

# This file contains all classes to parse data coming from NCP for SQC upload

from BaseUploader import ConditionData
from shutil       import copyfile
from copy         import deepcopy
from Utils        import search_files, check_ot_module_label
from Exceptions   import *


class NCP(ConditionData):
   """Produces the XML file to upload NCP SQC IV data."""

   summary_data   = {'cond_name' : 'Tracker Strip-Sensor Summary Data',
                     'table_name': 'TEST_SENSOR_SMMRY',                   
                     'DBvar_vs_TxtHeader' : {
                                 'AV_TEMP_DEGC'     : 'AV_TEMP_DEGC',
                                 'AV_RH_PRCNT'      : 'AV_RH_PRCNT'
                                            }
                    }

   data_description  = {"IV":{'cond_name'    : 'Tracker Strip-Sensor IV Test',
                        'table_name'   : 'TEST_SENSOR_IV',
                        'DBvar_vs_TxtHeader' : {
                                             'VOLTS'  : 'Voltage [V]',
                                             'CURRNT_NAMP': 'Current [A]',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'

                                             },
                               'mandatory' : ['VOLTS','CURRNT_NAMP']},
                        
                        "CV": {'cond_name' : 'Tracker Strip-Sensor CV Test',
                        'table_name': 'TEST_SENSOR_CV',
                        'DBvar_vs_TxtHeader' : {
                                             'VOLTS'  : 'Voltage [V]',
                                             'CAPCTNC_PFRD' : 'Capacitance[F]',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'

                                            },
                              'mandatory' : ['VOLTS','CAPCTNC_PFRD']},

                        "CINT": {'cond_name' : 'Tracker Strip-Sensor CIS Test',
                        'table_name': 'TEST_SENSOR_CIS',
                        'DBvar_vs_TxtHeader' : {
                                             'STRIPCOUPLE'  : 'Pad no.',
                                             'CAPCTNC_PFRD' : 'Cint Cp[F]',
                                             'BIASCURRNT_NAMPR':'Not in txt',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'
                                            },
                              'mandatory' : ['STRIPCOUPLE','CAPCTNC_PFRD']},
                                            
                        "RINT": {'cond_name' : 'Tracker Strip-Sensor RIS Test',
                        'table_name': 'TEST_SENSOR_RIS',
                        'DBvar_vs_TxtHeader' : {
                                             'STRIPCOUPLE'  : 'Pad no.',
                                             'RESSTNC_GOHM' : 'Rint res[Ohm]',
                                             'BIASCURRNT_NAMPR':'Not in txt',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'
                                            },
                              'mandatory' : ['STRIPCOUPLE','RESSTNC_GOHM']},

                        "RPOLY": {'cond_name' : 'Tracker Strip-Sensor RS Test',
                        'table_name': 'TEST_SENSOR_RS',
                        'DBvar_vs_TxtHeader' : {
                                             'STRIP'  : 'Pad no.',
                                             'RESSTNC_MOHM' : 'Rpoly res[Ohm]',
                                             'BIASCURRNT_NAMPR':'Not in txt',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'

                                            },
                              'mandatory' : ['VOLTS','RESSTNC_MOHM']},


                        "ILEAK": {'cond_name' : 'Tracker Strip-Sensor IS Test',
                        'table_name': 'TEST_SENSOR_IS',
                        'DBvar_vs_TxtHeader' : {
                                             'STRIP'  : 'Pad no.',
                                             'CURRNT_NAMPR' : 'Istrip current[A]',
                                             'BIASCURRNT_NAMPR':'Not in txt',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'

                                            },
                              'mandatory' : ['VOLTS','CURRNT_NAMPR']},

                        "CC": {'cond_name' : 'Tracker Strip-Sensor CS Test',
                        'table_name': 'TEST_SENSOR_CS',
                        'DBvar_vs_TxtHeader' : {
                                             'STRIP'  : 'Pad no.',
                                             'CAPCTNC_PFRD' : 'Cac Cp[F]',
                                             'BIASCURRNT_NAMPR':'Not in txt',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'

                                            },
                              'mandatory' : ['VOLTS','CAPCTNC_PFRD']},

                        "PINHOLE": {'cond_name' : 'Tracker Strip-Sensor PHS Test',
                        'table_name': 'TEST_SENSOR_PHS',
                        'DBvar_vs_TxtHeader' : {
                                             'STRIP'  : 'Pad no.',
                                             'CURRNTPH_NAMP' : 'Idiel current [A]',
                                             'BIASCURRNT_NAMPR':'Not in txt',
                                             'TEMP_DEGC' : 'Temperature degree [C]',
                                             'RH_PRCNT'  : 'Humidity rel. percent[rel %]',
                                             'TIME' : 'Not in txt'

                                            },
                              'mandatory' : ['VOLTS','CURRNTPH_NAMP']}



                    }

   def __init__(self, pDataType, pMeasurementType, pRunDataReader, pSummaryDataReader, pMeasurementDataReader=None):

      """Constructor: it requires configuration of the upload and the measurement data
      pDataType:
         SUMMARY or DATA depending which block of the xml file is generated
      pRunDataReader:
         NameLabel
         Date
         Inserter
      pMeasurementDataReader: List of
         VOLTS
         CURRNT_NAMP
         TEMP_DEGC (optional)
         RH_PRCNT (optional)
         TIME (optional)
      """

      self.type   = pDataType
      sensor_name = pRunDataReader.getDataFromTag('# ID')
      inserter    = pRunDataReader.getDataFromTag('# Operator')
      date        = pRunDataReader.getDataFromTag("# Date")
      run_type    = "SQC"
      kind_of_part= "2S Sensor"
      location    = "Islamabad"
      configuration = {}
      data_description = self.data_description[pMeasurementType]

      configuration.update(deepcopy(data_description))
      configuration['kind_of_part']     = kind_of_part
      configuration['inserter']     = inserter
      configuration['name_label']       = sensor_name
      #configuration['run_comment']  = comment
      #configuration['data_comment'] = comment
      #configuration['run_location'] = location
      configuration['run_begin']    = date
      configuration['run_type']     = run_type
      configuration['run_location'] = location

      configuration['data_version'] = "1.0"
      configuration['attributes']   = [['Data Quality','Good']]  

      #print(configuration)
      if self.type == "SUMMARY":
         name = 'SUMMARY-{}'.format(sensor_name)
         configuration.update(self.summary_data)
         ConditionData.__init__(self, name, configuration, pSummaryDataReader)

      if self.type == "DATA":
         name = 'DATA-{}'.format(sensor_name)
         configuration.update(self.data_description)
         ConditionData.__init__(self, name, configuration, pMeasurementDataReader)