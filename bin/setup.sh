#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]:-${(%):-%x}}" )" >/dev/null 2>&1 && pwd )"

export PYTHONPATH=$PYTHONPATH:${DIR}/../modules:${DIR}/../resthub/clients/python/src/main/python:${DIR}/../cmsdbldr/src/main/python/

export RHAPI=${DIR}/../resthub/clients/python/src/main/python/rhapi.py
export DBLOADER=${DIR}/../cmsdbldr/src/main/python/cmsdbldr_client.py
export PATH=${PATH}:${DIR}
