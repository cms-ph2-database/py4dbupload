#!/bin/bash

echo 'Available connections: '
python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login

DB='trker_cmsr'
echo 'Connect to ' ${DB}
echo

if [ $# -eq 2 ]
then

PARTID="$1"
IDTYPE="$2"

echo "Gathering condition data attached to component ${PARTID}"
echo

python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login "select d.kind_of_condition as Condition, r.run_number as run, r.run_type, r.begin_date as run_date from ${DB}.datasets d left join ${DB}.runs r on d.run_id=r.id where d.part_id IN (select p.id from ${DB}.parts p where p.${IDTYPE}='${PARTID}') order by r.begin_date asc" --all -n | column -s, -t

else

#KIND="$*"
echo "Scripts need two arguments, the PART ID and the ID TYPE tag."
echo "   usage: conditions.sh 2S_40_6_BEL-00004 name_label"
echo "   id type are: name_label, serial_number, barcode" 

fi

