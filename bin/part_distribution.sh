#!/bin/bash

echo 'Available connections: '
python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login2

DB='trker_cmsr'
echo 'Connect to ' ${DB}
echo

if [ $# -eq 0 ]
then

python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login "select p.kind_of_part as type, nvl(l.location_name, 'IN A PACKAGE') as location, count(p.id) count  from ${DB}.parts p left join  ${DB}.trkr_locations_v l on  l.location_id=p.location_id where p.kind_of_part!='Tracker Detector ROOT' group by p.kind_of_part,l.location_name order by p.kind_of_part, l.location_name asc" --all -n | column -s, -t

else

KIND="$*"
echo "Retrieving only ${KIND}"
python3 ${RHAPI} --url=https://cmsdca.cern.ch/trk_rhapi --login "select p.kind_of_part as type, nvl(l.location_name, 'IN A PACKAGE') as location, count(p.id) count  from ${DB}.parts p left join  ${DB}.trkr_locations_v l on  l.location_id=p.location_id where p.kind_of_part = '${KIND}' group by p.kind_of_part,l.location_name order by l.location_name asc" --all -n | column -s, -t

fi

