#!/usr/bin/env python
# $Id$
# Created by Stefan Maier  <stefan.maier@cern.ch>, 20.02.25

# This script corrects the faulty serial numbers of CIC Chips.

import os,time,datetime, csv

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader
from Hybrid       import HybridComponentT,HybridResultsT
from SSA          import CICchipUpdate
from Query        import Query
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,DBaccess,search_files
from optparse     import OptionParser
from progressbar  import *


def generate_cic_label (lot_name , wafer_number,  row, col, pos):
    name = lot_name[:6] + "_" + "{:01d}".format(wafer_number) + "_CIC_" + "{:01X}".format(row) + "{:01X}".format(abs(col)) + "{:01X}".format(abs(pos))
    return name

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with correction data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   (opt, args) = p.parse_args()

   db = 'trker_cmsr'
   if opt.isDevelopment:
      db = 'trker_int2r'

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'


   corr = []
   lot = ""
   wafer =""
   print("Loaded correction file")

   cic_file_reader   = TableReader(opt.data_path, 'CIC Correction', d_offset=0, t_offset=1, tabSize=23)
   print ('\nCIC IDs file content',   cic_file_reader)
   cic_file_content= cic_file_reader.getDataAsCWiseDictRowSplit()
   [print(line) for line in cic_file_content]

   for entry in cic_file_content:
      chip_name = generate_cic_label(entry["Lot Name"], int(entry["Wafer"]), int(entry["row"]), int(entry["col"]), int(entry["pos"]) )
      corr.append({"name" : chip_name, "new_serial": entry["ID"]})
      lot = entry["Lot Name"][:6]
      wafer = entry["Wafer"]


   print("Search DB for CICs of given wafer")
   query = Query(db,'login' if not opt.twofa else 'login2', opt.verbose)
   data = query.getCicIdsOfWafer(lot, wafer)
   print("Found CICs of wafer ", lot, wafer)
   [print(point) for point in data]
   match = 0
   print("Search for matches")
   for corr_item in corr:
      for db_entry in data:
         if db_entry["nameLabel"] == corr_item["name"]:
            corr_item["id"] = db_entry["id"]
            corr_item["old_serial"] = db_entry.get("serialNumber","")
            print("found match ", corr_item )
            match += 1

   if match ==len(data):
      print("Found all CICs of wafer")
   else:
      print("Number of CICs in file ({}) is not the same as CICs found in the DB ({})".format(match, len(data)))
      exit(1)

   [print(point) for point in corr]

   cic = None
   update_serials_file = None
   cic_update = UploaderContainer('CICUpdate')
   for corr_item in corr:
      cic = CICchipUpdate({},{'serial_number':corr_item["new_serial"],'id':corr_item['id'] })
      cic_update.add(cic)

   file = cic_update.dump_xml_data()

   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                        login_type=BaseUploader.login,verbose=True)
   if opt.upload:
      print("Upload correction")
      db_loader.upload_data(file)
