#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 28-March-2024

# This script masters the registration of the IT Sensor components.

import os,traceback,sys,time

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader,scale,scale2
from ITSensor     import ITwafer, ITsensor, ITsensorMeasurements, IThalfmoon,\
                         IThalfmoonMeasurements, ITDataType, ITSensType,\
                         ITHRegions
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,search_files
from datetime     import date,datetime
from dateutil     import parser
from optparse     import OptionParser
from copy         import deepcopy
from decimal      import Decimal, ROUND_HALF_UP
from progressbar  import *

def sensor_measurements(file,run_data,summary_data,iv_data,cv_data,vendor_data):
   typ = vendor_data.getDataFromTag('#Sensor Type')
   if typ=='IT_Single_3D':
      iv_rev      = TableReader(file, d_offset=25,m_rows=81,tabSize=15,csv_delimiter="\t")
      iv_for      = TableReader(file, d_offset=109,m_rows=41,tabSize=15,csv_delimiter="\t")
      cv          = TableReader(file, d_offset=153,m_rows=31,tabSize=15,csv_delimiter="\t")
      IV_reverse  = ITsensorMeasurements(run_data,iv_data,ITDataType.iv,iv_rev,vendor_data)
      IV_reverse.invert_value = []
      IV_forward  = ITsensorMeasurements(run_data,iv_data,ITDataType.ivf,iv_for,vendor_data)
      IV_forward.invert_value = []
      CV          = ITsensorMeasurements(run_data,cv_data,ITDataType.cv,cv,vendor_data)
      CV.invert_value = []
      SMMRY = ITsensorMeasurements(run_data,summary_data,ITDataType.summary,cv,vendor_data)
      SMMRY.add_child( IV_reverse )
      SMMRY.add_child( IV_forward )
      SMMRY.add_child( CV )
      return SMMRY
   else:
      iv = TableReader(f, d_offset=26,m_rows=82,tabSize=15,csv_delimiter="\t")
      cv = TableReader(f, d_offset=109,m_rows=42,tabSize=15,csv_delimiter="\t")
      IV = ITsensorMeasurements(run_data,iv_data,ITDataType.iv,iv,vendor_data)
      CV = ITsensorMeasurements(run_data,cv_data,ITDataType.cv,cv,vendor_data)
      SMMRY = ITsensorMeasurements(run_data,summary_data,ITDataType.summary,None,vendor_data)
      SMMRY.add_child( IV )
      SMMRY.add_child( CV )
      return SMMRY
   
def halfmoon_measurements(file,run_data,summary_data,iv_data,cv_data,vendor_data):
   typ = vendor_data.getDataFromTag('#Sensor Type')
   if typ=='IT_CROC_SMALL' or typ=='IT_CROC_BIG':
      iv = TableReader(file, d_offset=25,m_rows=101,tabSize=15,csv_delimiter="\t")
      cv = TableReader(file, d_offset=129,m_rows=81,tabSize=15,csv_delimiter="\t")
      IV = IThalfmoonMeasurements(run_data,iv_data,ITDataType.iv,iv,vendor_data)
      CV = IThalfmoonMeasurements(run_data,cv_data,ITDataType.cv,cv,vendor_data)
      IV.invert_value = []
      CV.invert_value = []
      SMMRY = IThalfmoonMeasurements(run_data,summary_data,ITDataType.summary,None,vendor_data)
      SMMRY.add_child( IV )
      SMMRY.add_child( CV )
      return SMMRY
   else:
      iv = TableReader(f, d_offset=26,m_rows=82,tabSize=15,csv_delimiter="\t")
      cv = TableReader(f, d_offset=109,m_rows=42,tabSize=15,csv_delimiter="\t")
      IV = IThalfmoonMeasurements(run_data,iv_data,ITDataType.iv,iv,vendor_data)
      CV = IThalfmoonMeasurements(run_data,cv_data,ITDataType.cv,cv,vendor_data)
      SMMRY = IThalfmoonMeasurements(run_data,summary_data,ITDataType.summary,None,vendor_data)
      SMMRY.add_child( IV )
      SMMRY.add_child( CV )
      return SMMRY

def Upload(filename):
   import sys
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                      login_type=BaseUploader.login,verbose=opt.debug)
   sys.stdout.write(f'Uploading {filename} .... ')
   t = datetime.now()
   sys.stdout.flush()
   db_loader.upload_data(filename)
   elapsed_time = (datetime.now() - t).total_seconds()
   print(f'terminated in {elapsed_time} seconds')

         
if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with measurement data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   p.add_option( '--date',
               type    = 'string',
               default = '',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component production; overwrites the date in the cvs table.')
   
   (opt, args) = p.parse_args()

   if len(args)>2:
      p.error("accepts at most one argument!")


   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose = opt.verbose
   BaseUploader.debug = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'

   # write the description and the attributes for all the Wafer components
   wafer_conf = {
      'description'      : '', # write the user comment on the component here
      'attributes'       : [('Status','Good')],
   }

   # write the description and the attributes for all the Sensor components
   sensor_conf = {
      'description'      : '',# write the user comment on the component here
      'attributes'       : [('Status','Good')],# Status attributes are set inside for the 3D
   }

   # write the version and the description for the IV data
   iv_data = {
      'data_version'     : 'v1',
      'data_comment'     : '',    # write the user comment on the dataset here
             }

   # write the version and the description for the CV data
   cv_data = {
      'data_version'     : 'v1',
      'data_comment'     : '',    # write the user comment on the dataset here
             }

   # write the version and the description for the summary data
   summary_data = {
      'data_version'     : 'v1',
      'data_comment'     : '',    # write the user comment on the dataset here
                  }
   
   # pass the production date as a configuration parameter to overwrite the date in the csv file
   date_of_production = parser.parse(opt.date) if opt.date!='' else None
   if date_of_production!=None:
      wafer_conf ['product_date']=date.strftime(date_of_production,'%Y-%m-%d')
      sensor_conf['product_date']=date.strftime(date_of_production,'%Y-%m-%d')

   # gather excel files conatining the data measurements
   data_files = sorted( search_files(opt.data_path,'*.txt') )

   wafers       = UploaderContainer('Wafers')
   sensors      = UploaderContainer('Sensors')
   sensorData   = UploaderContainer('SensorData')
   halfmoons    = UploaderContainer('Halfmoons')
   halfmoonData = UploaderContainer('HalfmoonData')

   envelope_ids = None
   if len(args)!=0:  envelope_ids = TableReader(args[0])

   nFiles = len(data_files)
   bar = progressbar(range(nFiles), widgets=['Processing data files: ', \
                Bar('=', '[', ']'), ' ', Percentage()])

   for i in bar:
      f = data_files[i]
      sd = TableReader(f, d_offset=1, m_rows=25,tabSize=43,csv_delimiter="\t")
      #iv = TableReader(f, d_offset=26,m_rows=82,tabSize=15,csv_delimiter="\t")
      #cv = TableReader(f, d_offset=109,m_rows=42,tabSize=15,csv_delimiter="\t")
      
      # store wafer classes for registration
      wafer = ITwafer(wafer_conf,sd,envelope_ids)
      wafers.add(wafer)

      # store sensor / halfmoon classes for registration
      sensor_type = sd.getDataFromTag('#Sensor Type')
      try:
         ITSensType[str(sensor_type)]  # check if type is an Halfmoon cut
         sensor = ITsensor(sensor_conf,sd,envelope_ids)
         sensors.add(sensor)
      except:
         halfmoon = IThalfmoon(sensor_conf,sd,None,envelope_ids)
         halfmoons.add(halfmoon)

      # set run number and run comment
      run_data = {
         'run_type'         : 'VQC',
         'run_comment'      : '',    # put the user comment on the run here
                }

      # store sensor / halfmoon measurements
      try:
         ITSensType[str(sensor_type)]  # check if type is an Halfmoon cut
         sensorData.add( sensor_measurements(f,run_data,summary_data,iv_data,cv_data,sd) )
         #if sensor_type=='IT_Single_3D':
         #   ivr = TableReader(f, d_offset=26,m_rows=82,tabSize=15,csv_delimiter="\t")
         #   ivf = TableReader(f, d_offset=26,m_rows=82,tabSize=15,csv_delimiter="\t")
         #   cv  = TableReader(f, d_offset=109,m_rows=42,tabSize=15,csv_delimiter="\t")
         #   IVR = ITsensorMeasurements(run_data,iv_data,ITDataType.iv,ivr,sd)
         #   IVF = ITsensorMeasurements(run_data,iv_data,ITDataType.iv,ivf,sd)
         #   CV = ITsensorMeasurements(run_data,cv_data,ITDataType.cv,cv,sd)
         #   SMMRY = ITsensorMeasurements(run_data,summary_data,ITDataType.summary,cv,sd)
         #else:
         #   iv = TableReader(f, d_offset=26,m_rows=82,tabSize=15,csv_delimiter="\t")
         #   cv = TableReader(f, d_offset=109,m_rows=42,tabSize=15,csv_delimiter="\t")
         #   IV = ITsensorMeasurements(run_data,iv_data,ITDataType.iv,iv,sd)
         #   CV = ITsensorMeasurements(run_data,cv_data,ITDataType.cv,cv,sd)
         #   SMMRY = ITsensorMeasurements(run_data,summary_data,ITDataType.summary,cv,sd)
            #SMMRY.attach_part = False
         #   SMMRY.add_child( IV )
         #   SMMRY.add_child( CV )
         #sensorData.add(SMMRY)
      except:
         #IV = IThalfmoonMeasurements(run_data,iv_data,ITDataType.iv,iv,sd)
         #CV = IThalfmoonMeasurements(run_data,cv_data,ITDataType.cv,cv,sd)
         #SMMRY = IThalfmoonMeasurements(run_data,summary_data,ITDataType.summary,cv,sd)
         ##SMMRY.attach_part = False
         #SMMRY.add_child( IV )
         #SMMRY.add_child( CV )
         halfmoonData.add( halfmoon_measurements(f,run_data,summary_data,iv_data,cv_data,sd) )
         
      time.sleep(0.1)

   streams.flush()

   halfmoons_without_file=[]
   for wafer in wafers.names():
      if wafer.split('_')[1]!='3D':  # do not register all halfmoon for 3D
         for region in ITHRegions:
            hname = f'{wafer}_{region.name}'
            if hname not in halfmoons.names():
               halfmoons_without_file.append(hname)
   
   nHalfmoons = len(halfmoons_without_file)
   
   if nHalfmoons > 0:
      bar = progressbar(range(nHalfmoons), widgets=['Processing Halfmoons with no file: ', \
                Bar('=', '[', ']'), ' ', Percentage()])
      for i in bar:
         n = halfmoons_without_file[i]
         conf = deepcopy(sensor_conf)
         conf['name'] = n
         if any(n in s for s in halfmoons.names()):
            conf['attributes'].append(('IT Halfmoon Structure','Sub Cut'))
         else:
            conf['attributes'].append(('IT Halfmoon Structure','Full'))
      
         wafer_name = '_'.join(n.split('_')[:4])
         halfmoon = IThalfmoon(conf,None,wafers.find(wafer_name),)
         halfmoons.add(halfmoon)
   
         time.sleep(0.1)
   
   streams.flush()
   
   wafer_xml  = wafers.dump_xml_data() if len(wafers.uploaders)!=0 else None
   sensor_xml = sensors.dump_xml_data(wafers.uploaders) if len(sensors.uploaders)!=0 else None
   qcdata_xml = sensorData.dump_xml_zip() if len(sensorData.uploaders)!=0 else None
   halfmn_xml = halfmoons.dump_xml_data(wafers.uploaders) if len(halfmoons.uploaders)!=0 else None
   hmdata_xml = halfmoonData.dump_xml_zip() if len(halfmoonData.uploaders)!=0 else None
   
   #path = os.path.dirname(os.environ.get('DBLOADER'))
   #db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
   #                   login_type=BaseUploader.login,verbose=opt.debug)
   
   if opt.upload:
      # The test mode of the DB-Loader does not work for attributes 
      if wafer_xml!=None: Upload(wafer_xml)
         #print('Uploading wafers .... ',end="")
         #t = datetime.now()
         #db_loader.upload_data(wafer_xml)
         #elapsed_time = datetime.now() - t
         #print(f"terminated in {elapsed_time}")
      if sensor_xml!=None: Upload(sensor_xml)#db_loader.upload_data(sensor_xml)
      if qcdata_xml!=None: Upload(qcdata_xml)#db_loader.upload_data(qcdata_xml)
      if halfmn_xml!=None: Upload(halfmn_xml)#db_loader.upload_data(halfmn_xml)
      if hmdata_xml!=None: Upload(hmdata_xml)#db_loader.upload_data(hmdata_xml)