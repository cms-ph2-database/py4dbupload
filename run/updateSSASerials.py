#!/usr/bin/env python
# $Id$
# Created by Stefan Maier  <stefan.maier@cern.ch>, 18.02.25

# This script corrects the faulty serial numbers of SSA Chips.

import os,time,datetime, csv

from AnsiColor    import Fore, Back, Style
from DataReader   import TableReader
from Hybrid       import HybridComponentT,HybridResultsT
from SSA          import SSAchipUpdate
from Query        import Query
from BaseUploader import BaseUploader
from Utils        import UploaderContainer,DBupload,DBaccess,search_files
from optparse     import OptionParser
from progressbar  import *


def generate_ssa_label (lot_name , wafer_number,  row, col, pos):
    name = lot_name + "_" + "{:01d}".format(wafer_number) + "_" + "{:01X}".format(row) + "{:01X}".format(abs(col)) + "{:01X}".format(abs(pos))
    return name

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] ", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the txt files with correction data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')
   
   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database')
   
   (opt, args) = p.parse_args()

   db = 'trker_cmsr'
   if opt.isDevelopment:
      db = 'trker_int2r'

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'


   corr = []
   lot = ""
   wafer =""
   print("Loaded correction file")
   with open(opt.data_path) as csvfile:
      spamreader = csv.reader(csvfile)
      for i, row in enumerate(spamreader):
         if i > 0:
            #print(row)
            name = generate_ssa_label(row[1], int(row[3]), int(row[4]), int(row[5]), int(row[6])).replace(" ","")
            corr.append({"name" : name, "new_serial": str(row[8]).replace(" ","")})
            lot = row[1].replace(" ","")
            wafer = row[3].replace(" ","")


   print("Search DB for SSAs of given wafer")
   query = Query(db,'login' if not opt.twofa else 'login2', opt.verbose)
   data = query.getSsaIdsOfWafer(lot, wafer)
   print("Found SSAs of wafer ", lot, wafer)
   #[print(point) for point in data]
   match = 0

   print("Search for matches")
   for corr_item in corr:
      for db_entry in data:
         if db_entry["nameLabel"] == corr_item["name"]:
            corr_item["id"] = db_entry["id"]
            corr_item["old_serial"] = db_entry.get("serialNumber","")
            print("found match ", corr_item )
            match += 1

   if match ==len(data):
      print("Found all SSAs of wafer")
   else:
      print("Number of SSAs in file ({}) is not the same as SSAs found in the DB ({})".format(match, len(data)))
      exit(1)

   #[print(point) for point in corr]

   ssa = None
   update_serials_file = None
   ssa_update = UploaderContainer('SSAUpdate')
   for corr_item in corr:
      ssa = SSAchipUpdate({},{'serial_number':corr_item["new_serial"],'id':corr_item['id'] })
      ssa_update.add(ssa)

   file = ssa_update.dump_xml_data()

   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,\
                        login_type=BaseUploader.login,verbose=True)
   if opt.upload:
      print("Upload correction")
      db_loader.upload_data(file)
