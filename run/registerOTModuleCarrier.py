#!/usr/bin/env python
# $Id$
# Created by Andreas Mussgiller <andreas.mussgiller@cern.ch>, 22-Feb-2025

# This script masters the 2S and PS module carrier registration. It is normally
# invoked with one or more barecodes (option --barcode) and a location (option
# --location). Barcodes can also be contained in a csv file with a single
# headerless column of barecodes.

import os,sys,fnmatch
import pandas as pd

from AnsiColor  import Fore, Style
from Exceptions import *
from OTCarrier  import OTCarrierComponentT, BaseUploader
from Utils      import UploaderContainer, DBupload
from optparse   import OptionParser

valid_locations = ['Aachen 1B',
                    'Aachen 3B',
                    'Bari',
                    'Brown',
                    'Brussels ULB',
                    'Brussels VUB',
                    'DESY B25c Assembly',
                    'DESY B26 Integration',
                    'Fermilab',
                    'KIT',
                    'Perugia',
                    'Bhubaneshwar NISER',
                    'Rutgers',
                    'Islamabad',
                    'Louvain',
                    'Pisa',
                    'Princeton',
                    'Rutgers']

if __name__ == "__main__":
    p = OptionParser(usage="usage: %prog [options] [csv file name]", version="1.1")
    p.add_option('--date',
                 type    = 'string',
                 dest    = 'date',
                 metavar = 'STR',
                 help    = 'Production date')

    p.add_option('-l','--location',
                 type    = 'string',
                 default = None,
                 dest    = 'location',
                 metavar = 'STR',
                 help    = 'Location of the carriers to be registered.')

    p.add_option('-i','--inserter',
                 type    = 'string',
                 default = None,
                 dest    = 'inserter',
                 metavar = 'STR',
                 help    = 'Override the account name of the operator that performs the data update.')
   
    p.add_option('-c','--comment',
                 type    = 'string',
                 default = None,
                 dest    = 'comment',
                 metavar = 'STR',
                 help    = 'Comment to be added to the database.')
   
    p.add_option('--dev',
                 action  = 'store_true',
                 default = False,
                 dest    = 'isDevelopment',
                 help    = 'Set the development database as target.')

    p.add_option('--2fa',
                 action  = 'store_true',
                 default = False,
                 dest    = 'twofa',
                 help    = 'Set the two factor authentication login.')
   
    p.add_option('--upload',
                 action  = 'store_true',
                 default = False,
                 dest    = 'upload',
                 help    = 'Perform the data upload in database')
   
    p.add_option('--verbose',
                 action  = 'store_true',
                 default = False,
                 dest    = 'verbose',
                 help    = 'Force the uploaders to print their configuration and data')

    p.add_option('--debug',
                 action  = 'store_true',
                 default = False,
                 dest    = 'debug',
                 help    = 'Force the verbose options in the network query uploaders to print their configuration and data')
 
    (opt, args) = p.parse_args()

    if len(args)>1:
        p.error('too many arguments!')
          
    # Load barcodes from csv file
    if len(args)==1:
        df = pd.read_csv(args[0], header=None)
        opt.barcodes = df[df.columns[0]].tolist()   

    # Check date format

    BaseUploader.verbose = opt.verbose
    BaseUploader.debug = opt.debug
    BaseUploader.login = 'login' if not opt.twofa else 'login2'

    # Getting the part data from database
    BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'


    locationCount = len(valid_locations)
    print("------------------------------")
    if opt.location:
        if opt.location not in valid_locations:
            print(Fore.BLUE + f'Location must be one of the following: {", ".join(valid_locations)}')
            print(Fore.BLUE+'exiting ....'+Style.RESET_ALL)
        location = opt.location
    else:
        print("Valid locations")
        [print(i, valid_locations[i]) for i in range(locationCount)]
        locationNumber = input("Please type in NUMBER of your location: ")
        number = 0

        try:
            number = int(locationNumber)
            print(f"You entered {number}")
            if not (0 < number < locationCount):
                print(Fore.BLUE+f'Value out of range. Please enter a valid integer between 0 and {locationCount}.')
                print(Fore.BLUE+'exiting ....'+Style.RESET_ALL)
                exit(1)
        except ValueError:
            print(Fore.BLUE+f'This is not an integer. Please enter a valid integer between 0 and {locationCount}.')
            print(Fore.BLUE+'exiting ....'+Style.RESET_ALL)

            exit(1)
        location = valid_locations[number]
    
    print("Location set: " + location)
    print("------------------------------")
    if opt.date:
        production_date = opt.date
    else:
        production_date = input("Please type in the production date of the carriers (Format YYYY-MM-DD): ")

    import datetime
    try:
        datetime.datetime.strptime(production_date, '%Y-%m-%d')
    except ValueError:
        print(Fore.BLUE+f'Incorrect data format ({opt.date}) , should be YYYY-MM-DD')
        print(Fore.BLUE+'exiting ....'+Style.RESET_ALL)
        sys.exit(1)

    print(f'Production date: {production_date}')
    print("------------------------------")

    print("Entering scanning loop")

    elements = UploaderContainer('OTCarriers',opt.inserter)

    keepScanning = True
    while keepScanning:
        scan = input("Scan Carrier label (ENTER to EXIT and upload): ")
        if scan == "":
            break
        carrier = OTCarrierComponentT(scan,location,production_date,opt.comment)
        carrier.load_attribute('Status','Good')
        elements.add(carrier)
            
    elements.dump_xml_data()

    path = os.path.dirname(os.environ.get('DBLOADER'))
    db = DBupload(database=BaseUploader.database,path_to_dbloader_api=path,
                login_type=BaseUploader.login ,verbose=opt.debug)

    if opt.upload:
        # The test mode of the DB-Loader does not work for attributes 
        db.upload_data('OTCarriers.xml',not opt.upload)
