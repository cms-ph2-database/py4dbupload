########################################################################### generall things ###########################################################################
from optparse    import OptionParser
from Query import Query
from OTModulePDF import OTModulePDF
import os
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image as RLImage, Table, TableStyle
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from PIL import Image


import os

from AnsiColor   import Fore, Back, Style
from DataReader  import TableReader,scale,scale2
from Sensor      import *
from Utils       import *
from optparse    import OptionParser
from decimal     import Decimal, ROUND_HALF_UP
from progressbar import *
from OT2SModule  import OT2SModule


if __name__ == "__main__":
    p = OptionParser(usage="usage: %prog [options] ", version="1.1")
        #defines the module that is to be analysed
    p.add_option('--module',
                    type    = 'string',
                    default = None,
                    dest    = 'module',
                    metavar = 'STR',
                    help    = 'Get data for module')
        #defines the name of the output file if you want to name it something other than ‘OT_Moduleinformation_{name of module}.pdf’
    p.add_option('--output',
                    type    = 'string',
                    default = None,
                    dest    = 'output',
                    metavar = 'STR',
                    help    = 'output file')

    p.add_option( '--2fa',
                action  = 'store_true',
                default = False,
                dest    = 'twofa',
                help    = 'Set the two factor authentication login.')


    p.add_option( '--dev',
                action  = 'store_true',
                default = False,
                dest    = 'isDevelopment',
                help    = 'Set the development database as target.')
    (opt, args) = p.parse_args()

    BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
    BaseUploader.login = 'login' if not opt.twofa else 'login2'


    #get the name of the module from the terminal input and strip it off all ' and [] in order to get the bare name
    module = opt.module
        #check if there is a special name wanted for the output pdf and set it
    output  = opt.output
       
        #create new folder to save pdf and plots in
    base_dir    = '../OTModuleInfos'
    module_dir  = os.path.join(base_dir, f'OTModuleInfo_{module}')
    os.makedirs(module_dir, exist_ok=True)

    #initialize the classes
        #queries to get the data from the CMS database
    query = Query('trker_cmsr','login' if not opt.twofa else 'login2')
        #class to process the data and create the pictures 
    OTmodProp = OTModulePDF(module_dir)

    print("\n#############################################################################\nStarting the queries:\n")
    
    ############################################ functions to access the database and get the data needed for the given module ############################################   

        #get the names of the used components (module, l-feh, r-feh, sh, t-sensor, b-sensor), their qr codes and some basic information
    data = None
    module_components = query.get_ot_module_information_2s(module)   
    module, l_feh, r_feh, t_sensor, b_sensor, sh,vtrx, status, comments, spacing, manufacturer = OTmodProp.names_qrImage(module_components)
    

        #get the dicing of the sensors
    data_t = None #top sensor
    data_t = query.get_ot_sensor_metrology(t_sensor)
    data_b = None #bottom sensor
    data_b = query.get_ot_sensor_metrology(b_sensor)

    dicing_df = OTmodProp.dicing(data_t, data_b)


        #get the metrology information of the module
    data = None
    data = query.get_ot_module_metrology(module)

    mtrlgy_kind, xShift, yShift, rotation = OTmodProp.metrology_module(data)


        #get the iv measurements
    data_top        = None #top sensor
    data_bottom     = None #bottom sensor
    data_module     = None #module
    data_top        = query.get_ot_sensor_iv(t_sensor)
    data_bottom     = query.get_ot_sensor_iv(b_sensor)
    data_module     = query.get_ot_module_iv(module)

    #[print(d) for d in data_bottom]

    sensor_iv_plot_data = []
    module_iv_plot_data = []
    plot_colors = ['springgreen','red','steelblue','darkmagenta','firebrick','lightseagreen']
    i = 0
    for run_type in ["VQC" , "SQC", "mode_bareSensor", "mod_tailEncapsulated"]:
        voltage, scaled_current, current = OTmodProp.iv(data_top, run_type)
        if voltage is not None:
            sensor_iv_plot_data.append({"voltage": voltage, "current": scaled_current, "color": plot_colors[i], "label": f"Top Sensor {run_type}"})
            i += 1
    for run_type in ["VQC" , "SQC", "mode_bareSensor", "mod_tailEncapsulated"]:
        voltage, scaled_current, current = OTmodProp.iv(data_bottom, run_type)
        #print(voltage, run_type)
        if voltage is not None:
            #print("APPEND " + run_type )
            sensor_iv_plot_data.append({"voltage": voltage, "current": scaled_current, "color": plot_colors[i], "label": f"Bottom Sensor {run_type}"})
            i += 1
    i=0
    for run_type in ["mod_final","mod_bonded"]:
        voltage, scaled_current, current = OTmodProp.iv(data_module, run_type)
        if voltage is not None:
            module_iv_plot_data.append({"voltage": voltage, "current": scaled_current, "color": plot_colors[i], "label": f"Module {run_type}"})
            i += 1


    path_to_sensor_plot = OTmodProp.iv_plot(sensor_iv_plot_data, title = "Sensor")
    path_to_module_plot = OTmodProp.iv_plot(module_iv_plot_data, title = "Module")

    data = None
    data = query.wire_bond_pull(module)
    
    pulltest = OTmodProp.pulltest(data)

################################################################# arranging the plots on a pdf #################################################################
    #create the pdf with the specified output or standardized name and some general style definitions
    if output != None:
        doc  = SimpleDocTemplate(f"{module_dir}/{output}.pdf", pagesize=A4, rightMargin=1*cm, leftMargin=1*cm, topMargin=0.5*cm, bottomMargin=0.5*cm)
    else:
        doc  = SimpleDocTemplate(f"{module_dir}/OT_Moduleinformation_{module}.pdf", pagesize=A4, rightMargin=1*cm, leftMargin=1*cm, topMargin=0.5*cm, bottomMargin=0.5*cm)

    styles = getSampleStyleSheet()
    flowables = []

    #defining font styles
    normal_style = ParagraphStyle(
                                    "CustomNormal",
                                    parent=styles["Normal"],
                                    fontSize=9,  
                                    fontName="Helvetica",
                                )
    subtitle_style = ParagraphStyle(
                                    "CustomTitle",
                                    parent=styles["Normal"],
                                    fontSize=9,  
                                    fontName="Helvetica-Bold", 
                                )
    title_style = ParagraphStyle(
                                    "CustomTitle",
                                    parent=styles["Normal"],
                                    fontSize=11,  
                                    fontName="Helvetica-Bold",  
                                )

    #filling the white pdf page with content
        #title
    title = Paragraph(f" Properties of Module {module} - Overview", title_style)
    mod_qr_img = RLImage(f"{module_dir}/mod_qr.jpeg", width= 0.75*cm, height = 0.75*cm) if os.path.exists(f"{module_dir}/mod_qr.jpeg") else ""
    title_table = Table([[title, mod_qr_img]], colWidths=[11*cm,8*cm])
    flowables.append(title_table)
    flowables.append(Spacer(1,11))


        #module properties (status, spacing of the module, manufacturer, possible comments)
    module_prop = f"""Module Properties: """
    flowables.append(Paragraph(module_prop, subtitle_style))
    mod_prop_table_data =   [
                            ["status:", f"{status}", "", "spacing:", f"{spacing}", "", "manufacturer:", f"{manufacturer}"],
                            ]
    mod_prop_table = Table(mod_prop_table_data, colWidths=[2*cm, 2*cm, 3*cm, 2*cm, 2*cm, 3*cm, 2.5*cm, 2.5*cm])
    mod_prop_table.setStyle(TableStyle([('ALIGN', (0, 0), (-1, -1), 'LEFT'),
                            ('FONTNAME', (0, 0), (-1, -1), 'Helvetica'),
                            ('FONTSIZE', (0, 0), (-1, -1), 9)]))
    flowables.append(mod_prop_table)
    module_comment = f"""comments:  {comments}"""
    flowables.append(Paragraph(module_comment, normal_style))
    flowables.append(Spacer(1, 11))


        #components and their properties (dicing)
    sensor = f""" Components Used and Sensor Dicing: """
    flowables.append(Paragraph(sensor, subtitle_style)) 
            #names and qr codes of the components
    cmp_table_data = [
                        ["service hybrid:", f"{sh}", RLImage(f"{module_dir}/sh_qr.jpeg", width=0.75*cm, height=0.75*cm) if os.path.exists(f"{module_dir}/sh_qr.jpeg") else "", "VTRx+:", f"{vtrx}", RLImage(f"{module_dir}/vtrx_qr.jpeg", width=0.75*cm, height=0.75*cm) if os.path.exists(f"{module_dir}/vtrx_qr.jpeg") else ""],
                        ["left front-end hybrid:", f"{l_feh}", RLImage(f"{module_dir}/lfeh_qr.jpeg", width=0.75*cm, height=0.75*cm)  if os.path.exists(f"{module_dir}/lfeh_qr.jpeg") else "", "right front-end hybrid:", f"{r_feh}", RLImage(f"{module_dir}/rfeh_qr.jpeg", width=0.75*cm, height=0.75*cm) if os.path.exists(f"{module_dir}/rfeh_qr.jpeg") else ""],
                        ["sensor top:", f"{t_sensor}", RLImage(f"{module_dir}/tsen_qr.jpeg", width=0.75*cm, height=0.75*cm) if os.path.exists(f"{module_dir}/tsen_qr.jpeg") else "", "sensor bottom:", f"{b_sensor}", RLImage(f"{module_dir}/bsen_qr.jpeg", width=0.75*cm, height=0.75*cm)  if os.path.exists(f"{module_dir}/bsen_qr.jpeg") else ""],
                    ]   
            #append colored dicing values of the sensors
    for row in dicing_df.itertuples(index=False):
        top_color = OTmodProp.get_color_dicing(row[1])
        bottom_color = OTmodProp.get_color_dicing(row[2])
        cmp_table_data.append([Paragraph(f'<font color="{colors.black}">{row[0][:-1]}<sub>{row[0][-1]}</sub></font>', normal_style), 
                            Paragraph(f'<font color="{top_color}">{row[1]:.1f}</font>', normal_style), "", "",  
                            Paragraph(f'<font color="{bottom_color}">{row[2]:.1f}</font>', normal_style),
                            ])
            #create a table with the data filled in the cmp_table_data
    cmp_table = Table(cmp_table_data, colWidths=[3.5*cm, 4*cm, 2*cm, 3.5*cm, 4*cm, 2*cm])
    cmp_table.setStyle(TableStyle([ ('ALIGN', (0,0), (-1,-1), 'LEFT'),
                                    ('FONTNAME', (0, 0), (-1, -1), 'Helvetica'),
                                    ('FONTSIZE', (0, 0), (-1, -1), 9),
                                ]))

    flowables.append(cmp_table)
    flowables.append(Spacer(1, 11))


        #module metrology
    module_mtrlgy = f"""Module Metrology: """
    flowables.append(Paragraph(module_mtrlgy, subtitle_style))
    mod_mtrlgy_table_data = [
                                ['measurement kind: ', f'{mtrlgy_kind}'],
                                ['shift in x direction:', Paragraph(f'<font color = "{OTmodProp.shift_col(xShift)}">{xShift:.1f}</font> μm', normal_style), '', 'shift in y direction:', Paragraph(f'<font color = "{OTmodProp.shift_col(yShift)}">{yShift:.1f}</font> μm', normal_style), '', 'rotation:', Paragraph(f'<font color = "{OTmodProp.rot_col(rotation)}">{rotation:.1f}</font> μm', normal_style)],
                            ]
    mod_mtrlgy_table = Table(mod_mtrlgy_table_data, colWidths=[3*cm, 3*cm, 0.5*cm, 3*cm, 3*cm, 0.5*cm, 3*cm, 3*cm])
    mod_mtrlgy_table.setStyle(TableStyle([  ('ALIGN', (0,0), (-1,-1), 'LEFT'),
                                            ('FONTNAME', (0, 0), (-1, -1), 'Helvetica'),
                                            ('FONTSIZE', (0, 0), (-1, -1), 9)
                                        ]))
    flowables.append(mod_mtrlgy_table)
    flowables.append(Spacer(1, 11))


        #include plots
    if True:
        iv_plots = f"""IV Measurements: """
        flowables.append(Paragraph(iv_plots, subtitle_style))

        sensor_iv = RLImage(path_to_sensor_plot, width=9*cm, height=9*cm) if os.path.exists(path_to_sensor_plot) else Paragraph(f'<font color ="{colors.red}">{"NO SENSOR IV"}</font>', normal_style)
        module_iv = RLImage(path_to_module_plot, width=9*cm, height=9*cm) if os.path.exists(path_to_module_plot) else Paragraph(f'<font color ="{colors.red}">{"NO SENSOR AND MODULE IV"}</font>', normal_style)
        table_data = [[sensor_iv, "", module_iv]]
        plots = Table(table_data, colWidths=[9*cm, 1*cm, 9*cm])
        plots.setStyle(TableStyle([
                                    ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                    ('VALIGN', (0, 0), (-1, -1), 'MIDDLE')
                                ]))
        flowables.append(plots)
        """
        if no_top_iv == 1:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"There is no iv measurement for the top sensor"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        if no_bottom_iv == 1:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"There is no iv measurement for the bottom sensor"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        if no_bonded_iv == 1:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"There is no data for the bonded module"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        if no_final_iv == 1:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"There is no data for the final module"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        if sen_scaling_top == 0:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"No scaling possible for the top sensor"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        if sen_scaling_bot == 0:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"No scaling possible for the bottom sensor"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        if mod_scaling_bonded == 0:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"No scaling possible for the bonded module"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        if mod_scaling_final == 0:
            flowables.append(Spacer(1,5))
            flowables.append(Paragraph(f'<font color ="{colors.red}">{"No scaling possible for the final module"}</font>', normal_style))
            flowables.append(Spacer(1,5))
        flowables.append(Spacer(1,11))
        """        
    else:
        plots = f"""There are no IV meassurements"""
        flowables.append(Paragraph(f'<font color ="{colors.red}">{plots}</font>', subtitle_style))

        #wirebond pull tests
    pulltest_title = f""" Wire-Bond Pull Tests: """
    flowables.append(Paragraph(pulltest_title, subtitle_style))

    pulltest_table_data = [
        ['', 'sensor', 'hybrid', '', 'sensor', 'hybrid'],
        ['top left:', OTmodProp.get_formatted_value(pulltest, 2, 'Sensor'), OTmodProp.get_formatted_value(pulltest, 2, 'Hybrid'), 'top right:', OTmodProp.get_formatted_value(pulltest, 3, 'Sensor'), OTmodProp.get_formatted_value(pulltest, 3, 'Hybrid')],
        ['bottom left:', OTmodProp.get_formatted_value(pulltest, 0, 'Sensor'), OTmodProp.get_formatted_value(pulltest, 0, 'Hybrid'), 'bottom right:', OTmodProp.get_formatted_value(pulltest, 1, 'Sensor'), OTmodProp.get_formatted_value(pulltest, 1, 'Hybrid')]
    ]

    formatted_table_data = [[Paragraph(cell, normal_style) if isinstance(cell, str) and cell.startswith('<font') else cell for cell in row] for row in pulltest_table_data]

    pulltest_table = Table(formatted_table_data, colWidths=[2.5*cm, 3.5*cm, 3.5*cm, 2.5*cm, 3.5*cm, 3.5*cm])
    pulltest_table.setStyle(TableStyle([
        ('ALIGN', (0, 0), (-1, -1), 'LEFT'),
        ('FONTNAME', (0, 0), (-1, 0), 'Helvetica'),
        ('FONTSIZE', (0, 0), (-1, -1), 9)
    ]))

    flowables.append(pulltest_table)


    doc.build(flowables)

##################################################################################################################################    
    print("\nDone! Have fun with the PDF ;)\n#############################################################################\n")