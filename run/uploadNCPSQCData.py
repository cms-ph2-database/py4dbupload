#!/usr/bin/env python
# $Id$
# Created by Stefan Maier <s.maier@kit.edu>, 22-January-2024

# This script masters the upload of OT sensor probing data for NCP

#import os,sys,fnmatch

#from AnsiColor  import Fore, Back, Style
#from Exceptions import *
from Utils        import search_files,DBupload,UploaderContainer
from DataReader   import TableReader
from BaseUploader import BaseUploader
from SQCatNCP import NCP
from optparse     import OptionParser
from progressbar  import *
import csv
from datetime import datetime

import os,time

if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <measurement data file> ", version="1.1")

   p.add_option( '--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the tables with the measurement data.')

   p.add_option( '-c', '--config',
                type = 'string',
                default = None,
                dest    = 'config_file',
                metavar = 'STR',
                help    = 'Configuration file containing information about inserter, location, etc')

   p.add_option( '-i','--inserter',
               type    = 'string',
               default = None,
               dest    = 'inserter',
               metavar = 'STR',
               help    = 'Overwirtes the account name put in the RECORD_INSERTION_USER column.')
   
   p.add_option( '--dev',
               action  = 'store_true',
               default = False,
               dest    = 'isDevelopment',
               help    = 'Set the development database as target.')
   
   p.add_option( '--update',
               action  = 'store_true',
               default = False,
               dest    = 'update',
               help    = 'Force the upload of components in update mode')
   
   p.add_option( '--upload',
               action  = 'store_true',
               default = False,
               dest    = 'upload',
               help    = 'Perform the data upload in database.')

   p.add_option( '--store',
               action  = 'store_true',
               default = False,
               dest    = 'store',
               help    = 'Store the generated XML/ZIP file. Otherwise, delete it immediately')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data.')

   p.add_option( '--debug',
               action  = 'store_true',
               default = False,
               dest    = 'debug',
               help    = 'Force the verbose options in the network query uploaders to print their configuration and data.')

   p.add_option( '--2fa',
               action  = 'store_true',
               default = False,
               dest    = 'twofa',
               help    = 'Set the two factor authentication login.')

   (opt, args) = p.parse_args()

   if len(args)>1:
      p.error('accepts at most 1 argument!')

   BaseUploader.database = 'cmsr' if opt.isDevelopment==False else 'int2r'
   BaseUploader.verbose  = opt.verbose
   BaseUploader.debug    = opt.debug
   BaseUploader.login = 'login' if not opt.twofa else 'login2'
   
   data_file  = opt.data_path
   data_offset = 8

   file_type= "IV"

   files_to_be_uploaded = []
   #First open the file and check where the data block starts. Then check the file type: IV, CV, 2Param, 4Param
   with open(opt.data_path) as inputfile:
      lines = [line.rstrip() for line in inputfile]

      for index, line in enumerate(lines):
         #print(val)
         if 0 < index <len(lines):
            if lines[index-1] == '' and lines[index] != '':
               data_offset = index
               split = lines[index].split("\t") 
               if len(split) == 4 and split[1] == 'Current [A]':
                  file_type = "IV"
               if len(split) == 4 and split[1] == 'Capacitance[F]':
                  file_type = "CV"
               if len(split) == 5:
                  file_type = "2Param"
               if len(split) == 7:
                  file_type = "4Param"
               break
   #Parse file and generate temporary file with new header and corrected timestamp
   #Collect temperature and humidity values to fill out summary values
   temp_csv = 'temp.csv'
   temperatures = []
   humidities = []
   with open(opt.data_path,newline='\n', encoding='utf-8', errors='ignore') as csvfile: #'rb') as csvfile:
      datasheet = csv.reader((line.replace('\0','') for line in csvfile) , delimiter=":")
      with open(temp_csv, 'w', newline='') as tempfile:
         spamwriter = csv.writer(tempfile, delimiter=';')
         for i, row in enumerate(datasheet):
            if i < 5:
               spamwriter.writerow([row[0],row[1]])
            elif i == 5:
               date=row[1]+ ":" + row[2]+ ":" +row[3]
               date = date.strip()
               paresed_date= datetime.strptime(date, "%a %b %d %H:%M:%S %Y")
               spamwriter.writerow([row[0],datetime.strftime(paresed_date, "%Y-%m-%d %M:%M:%S")])

            #Store humidities and temperatures
            if i > data_offset and len(row[0].split("\t")[2]) > 1:
               temperatures.append(float(row[0].split("\t")[-2]))
               humidities.append(float(row[0].split("\t")[-1]))

   print("Mean Temperature: " , sum(temperatures)/len(temperatures))
   print("Mean Humidity: "    , sum(humidities)/len(humidities))

   #open temporary file and add summary block, then add all data lines with corrected units
   with open(temp_csv, 'a', newline='') as tempfile:
      spamwriter = csv.writer(tempfile, delimiter=';')
      spamwriter.writerow([])
      spamwriter.writerow(["AV_TEMP_DEGC", "AV_RH_PRCNT"])
      spamwriter.writerow([sum(temperatures)/len(temperatures),  sum(humidities)/len(humidities)])
      spamwriter.writerow([])
      with open(opt.data_path,newline='\n', encoding='utf-8', errors='ignore') as csvfile: #'rb') as csvfile:
         datasheet = csv.reader((line.replace('\0','') for line in csvfile) , delimiter="\t")
         for i, row in enumerate(datasheet):
            if '' in row: continue
            if data_offset - 3 < i < data_offset:
               spamwriter.writerow([])

            if i == data_offset:
               spamwriter.writerow(row)
            #upload nA, not A

            if i > data_offset:
               if file_type =="IV" :
                  spamwriter.writerow([row[0], str("{:.5f}".format(float(row[1])*1E9)), row[2], row[3] ])
               if file_type =="CV" :
                  spamwriter.writerow([row[0], str("{:.5f}".format(float(row[1])*1E12)), row[2], row[3] ])
               if file_type =="2Param" :
                  spamwriter.writerow([
                     row[0],
                     str("{:.5f}".format(float(row[1])*1E-9)),
                     str("{:.5f}".format(float(row[2])*1E12)),
                     row[3],
                     row[4] ])
               if file_type =="4Param" :
                  spamwriter.writerow([
                     row[0], 
                     str("{:.5f}".format(float(row[1])*1E9)), 
                     str("{:.5f}".format(float(row[2])*1E-6)), 
                     str("{:.5f}".format(float(row[3])*1E12)), 
                     str("{:.5f}".format(float(row[4])*1E9)), 
                     row[5], 
                     row[6] ])



   runinfo_reader   = TableReader(temp_csv, d_offset=0, m_rows=6, csv_delimiter=';', tabSize=25)
   summary_reader   = TableReader(temp_csv, d_offset=7, m_rows=1, csv_delimiter=';', tabSize=25)
   data_reader      = TableReader(temp_csv, d_offset=10, csv_delimiter=';', tabSize=25)

   NCP_Container = UploaderContainer(os.path.dirname(data_file) + '/' +'NCP_Sensor_{}'.format(file_type))
   print ('\nRun info data to upload',   runinfo_reader)
   print ('\nSummary data to upload',   summary_reader)
   print ('\nData to upload', data_reader)

   #Get the three data components from the csv file
   if file_type =="IV" :
      SMMRY = NCP("SUMMARY", "IV", runinfo_reader, summary_reader)
      DATA  = NCP("DATA",    "IV", runinfo_reader, summary_reader, data_reader)

      SMMRY.add_child( DATA )
      time.sleep(0.1)
      NCP_Container.add(SMMRY)

   if file_type =="CV" :
      SMMRY = NCP("SUMMARY", "CV", runinfo_reader, summary_reader)
      DATA  = NCP("DATA",    "CV", runinfo_reader, summary_reader, data_reader)

      SMMRY.add_child( DATA )
      time.sleep(0.1)
      NCP_Container.add(SMMRY)

   if file_type =="2Param" :
      SMMRY = NCP("SUMMARY", "RINT", runinfo_reader, summary_reader)
      DATA1  = NCP("DATA",    "RINT", runinfo_reader, summary_reader, data_reader)
      DATA2  = NCP("DATA",    "CINT", runinfo_reader, summary_reader, data_reader)

      SMMRY.add_child( DATA1 )
      time.sleep(0.1)
      SMMRY.add_child( DATA2 )
      time.sleep(0.1)

      NCP_Container.add(SMMRY)

   if file_type =="4Param" :
      SMMRY = NCP("SUMMARY", "ILEAK", runinfo_reader, summary_reader)
      DATA1  = NCP("DATA",    "ILEAK", runinfo_reader, summary_reader, data_reader)
      DATA2  = NCP("DATA",    "RPOLY", runinfo_reader, summary_reader, data_reader)
      DATA3  = NCP("DATA",    "CC", runinfo_reader, summary_reader, data_reader)
      DATA4  = NCP("DATA",    "PINHOLE", runinfo_reader, summary_reader, data_reader)

      SMMRY.add_child( DATA1 )
      time.sleep(0.1)
      SMMRY.add_child( DATA2 )
      time.sleep(0.1)
      SMMRY.add_child( DATA3 )
      time.sleep(0.1)
      SMMRY.add_child( DATA4 )
      time.sleep(0.1)

      NCP_Container.add(SMMRY)
   time.sleep(0.1)


   streams.flush()
   files_to_be_uploaded.append(NCP_Container.dump_xml_data(pSkipPartsBlock = True))

   # Upload files in the database 
   path = os.path.dirname(os.environ.get('DBLOADER'))
   db_loader = DBupload(database=BaseUploader.database,path_to_dbloader_api=path ,verbose=True)

   if opt.upload:
      for fi in files_to_be_uploaded:
         # Files upload
         db_loader.upload_data(fi)

   if not opt.store:
      for fi in files_to_be_uploaded:
         print("Remove temporary files")
         os.remove(fi)

